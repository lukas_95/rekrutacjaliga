﻿using System;
using System.Collections.Generic;

namespace rekrutacja.Models
{
    public partial class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Coach { get; set; }
        public int LeagueId { get; set; }
        public string Nation { get; set; }

        public virtual League League { get; set; }
    }
}
