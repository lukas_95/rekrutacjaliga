﻿using System;
using System.Collections.Generic;

namespace rekrutacja.Models
{
    public partial class League
    {
        public League()
        {
            Team = new HashSet<Team>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string Continent { get; set; }
        public string Discipline { get; set; }

        public virtual ICollection<Team> Team { get; set; }
    }
}
