﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace rekrutacja.Models
{
    public partial class SportContext : DbContext
    {
        public SportContext()
        {
        }

        public SportContext(DbContextOptions<SportContext> options)
            : base(options)
        {
        }

        public virtual DbSet<League> League { get; set; }
        public virtual DbSet<Team> Team { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<League>(entity =>
            {
                entity.Property(e => e.Continent)
                    .IsRequired()
                    .HasColumnName("continent")
                    .HasColumnType("text");

                entity.Property(e => e.Country)
                    .IsRequired()
                    .HasColumnName("country")
                    .HasColumnType("text");

                entity.Property(e => e.Discipline)
                    .IsRequired()
                    .HasColumnName("discipline")
                    .HasColumnType("text");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<Team>(entity =>
            {
                entity.Property(e => e.City)
                    .IsRequired()
                    .HasColumnName("city")
                    .HasColumnType("text");

                entity.Property(e => e.Coach)
                    .IsRequired()
                    .HasColumnName("coach")
                    .HasColumnType("text");

                entity.Property(e => e.LeagueId).HasColumnName("league_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("text");

                entity.Property(e => e.Nation)
                    .IsRequired()
                    .HasColumnName("nation")
                    .HasColumnType("text");

                entity.HasOne(d => d.League)
                    .WithMany(p => p.Team)
                    .HasForeignKey(d => d.LeagueId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Team_ToTable");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
