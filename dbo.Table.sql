﻿CREATE TABLE [dbo].[League] (
    [Id]         INT  IDENTITY (1, 1) NOT NULL,
    [name]       TEXT NOT NULL,
    [country]    TEXT NOT NULL,
    [continent]  TEXT NOT NULL,
    [discipline] TEXT NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

