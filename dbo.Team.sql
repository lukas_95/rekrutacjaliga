﻿CREATE TABLE [dbo].[Team] (
    [Id]        INT  IDENTITY (1, 1) NOT NULL,
    [name]      TEXT NOT NULL,
    [city]      TEXT NOT NULL,
    [coach]     TEXT NOT NULL,
    [league_id] INT  NOT NULL,
    [nation]    TEXT NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Team_ToTable] FOREIGN KEY ([league_id]) REFERENCES [dbo].[League] ([Id])
);

